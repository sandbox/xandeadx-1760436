<?php

/**
 * Implements hook_node_view_alter().
 */
function base_node_view_alter(&$build) {
  // Remove extra classes from node view render array
  if (theme_get_setting('base_commerce_pretty_classes')) {
    static $bundles_with_product_reference;
    if ($bundles_with_product_reference === NULL) {
      $bundles_with_product_reference = array();
      foreach (commerce_info_fields('commerce_product_reference', 'node') as $field) {
        if (!empty($field['bundles']['node'])) {
          $bundles_with_product_reference = array_merge($bundles_with_product_reference, $field['bundles']['node']);
        }
      }
    }

    if ($bundles_with_product_reference && in_array($build['#bundle'], $bundles_with_product_reference)) {
      foreach (element_children($build) as $key) {
        if (
          isset($build[$key]['#entity_type'], $build[$key]['#prefix']) &&
          $build[$key]['#entity_type'] == 'commerce_product' &&
          strpos($key, 'product:') === 0
        ) {
          $build[$key]['#prefix'] = '';
          $build[$key]['#suffix'] = '';
        }
      }

      if (isset($build['product:sku'])) {
        $build['product:sku']['#prefix'] = '';
        $build['product:sku']['#suffix'] = '';
      }
    }
  }
}

/**
 * Implements hook_commerce_cart_attributes_refresh_alter().
 */
function base_commerce_cart_attributes_refresh_alter(&$commands, &$form, &$form_state) {
  // Fix ajax commands
  if (theme_get_setting('base_commerce_pretty_classes')) {
    foreach ($commands as &$command) {
      if ($command['command'] == 'insert' && $command['method'] == 'replaceWith' && strpos($command['selector'], '.node-') !== FALSE) {
        $command['data'] = trim(preg_replace('/^<div class=".+?">(.+)<\/div>$/s', '$1', trim($command['data'])));

        if (preg_match('/ class="(.+?)[" ]/', $command['data'], $matches)) {
          $product_display_nid = $form_state['context']['entity_id'];
          $product_display = node_load($product_display_nid);
          $field_class_name = $matches[1];
          $command['selector'] = '.' . _base_get_entity_class_prefix('node', $product_display->type) . '-' . $product_display_nid . ' .' . $field_class_name;
        }
      }
    }
  }
}

/**
 * Preprocess vars for theme_commerce_price_formatted_components().
 */
function base_preprocess_commerce_price_formatted_components(&$vars) {
  if (
    isset($vars['components']['commerce_price_formatted_amount']['title']) &&
    $vars['components']['commerce_price_formatted_amount']['title'] = 'Order total'
  ) {
    $vars['components']['commerce_price_formatted_amount']['title'] = t('Order total');
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter: commerce_checkout_form.
 */
function base_form_commerce_checkout_form_alter(&$form, &$form_state) {
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['#type'])
    && $form[$key]['#type'] == 'fieldset'
    && empty($form[$key]['#collapsible'])
    && empty($form[$key]['#collapsed'])) {
      unset($form[$key]['#type']);
      $form[$key]['#theme'] = 'commerce_checkout_pane';

      $pane_suffix = str_replace(array('commerce_fieldgroup_pane__group_', 'commerce_'), '', $key);
      $form[$key]['#id'] = drupal_html_id('checkout-pane-' . $pane_suffix);
      $form[$key]['#attributes']['class'] = array(
        'form-wrapper',
        'checkout-pane',
        'checkout-pane-' . drupal_html_class($pane_suffix),
      );
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter(): commerce_checkout_form_complete.
 */
function base_form_commerce_checkout_form_complete_alter(&$form, &$form_state) {
  $form['checkout_completion_message']['message'] = array(
    '#theme' => 'commerce_checkout_completion_message',
    '#order' => $form_state['order'],
    '#message' => $form['checkout_completion_message']['message']['#markup'],
  );
}

/**
 * Override theme_commerce_checkout_review().
 */
function base_commerce_checkout_review($variables) {
  $form = $variables['form'];
  $output = '';

  foreach ($form['#data'] as $pane_id => $data) {
    $output .= '
      <div class="checkout-review-pane checkout-review-pane-' . drupal_html_class($pane_id) . '">
        <div class="checkout-review-pane-title">' . $data['title'] . '</div>
        <div class="checkout-review-pane-content">' . $data['data'] . '</div>
      </div>
    ';
  }

  return $output;
}

/**
 * Implements hook_form_FORM_ID_alter(): views_form_commerce_cart_form_default.
 */
function base_form_views_form_commerce_cart_form_default_alter(&$form, &$form_state) {
  if (isset($form['actions'])) {
    foreach (element_children($form['actions']) as $key) {
      $form['actions'][$key]['#attributes']['class'][] = 'cart-' . $key;
    }
  }
}
