<?php

if (module_exists('commerce')) {
  require 'template-commerce.php';
}

/**
 * Implements hook_theme().
 */
function base_theme() {
  return array(
    'jquery_tabs' => array(
      'variables' => array(
        'tabs' => NULL,
        'attributes' => NULL,
      ),
    ),
    'button_button' => array(
      'render element' => 'element',
    ),
    'views_exposed_widget' => array(
      'template' => 'templates/views-exposed-widget',
      'variables' => array(
        'widget' => NULL,
      ),
    ),
    'commerce_checkout_pane' => array(
      'template' => 'templates/commerce-checkout-pane',
      'render element' => 'form',
    ),
    'commerce_checkout_completion_message' => array(
      'template' => 'templates/commerce-checkout-completion-message',
      'variables' => array(
        'order' => NULL,
        'message' => NULL,
      ),
    ),
  );
}

/**
 * Implements hook_library().
 */
function base_library() {
  $base_theme_path = drupal_get_path('theme', 'base');

  $libraries['owl.carousel'] = array(
    'title' => 'Own Carousel',
    'website' => 'http://www.owlcarousel.owlgraphic.com/',
    'version' => '2',
    'js' => array(
      'sites/all/libraries/owlcarousel/dist/owl.carousel.js' => array(),
      $base_theme_path . '/js/base-owlcarousel.js' => array(),
      path_to_theme() . '/js/owlcarousel-presets.js' => array(),
    ),
    'css' => array(
      'sites/all/libraries/owlcarousel/dist/assets/owl.carousel.css' => array(),
      $base_theme_path . '/css/base-owlcarousel.css' => array(),
    ),
  );

  return $libraries;
}

/**
 * Preprocess vars for base_jquery_tabs().
 */
function base_preprocess_jquery_tabs(&$vars) {
  drupal_add_library('system', 'ui.tabs');
  drupal_add_js(drupal_get_path('theme', 'base') . '/js/base-tabs.js');
}

/**
 * Render jQuery UI Tabs.
 */
function base_jquery_tabs($vars) {
  $tabs_title = array();
  $tabs_content = array();

  foreach ($vars['tabs'] as $id => $info) {
    if (!$info['content']) {
      continue;
    }

    $tabs_title[] = '<a href="#' . $id . '">' . $info['title'] . '</a>';
    $tabs_content[] = '
      <div id="' . $id . '" class="tabs-panel">
        <div class="tabs-panel-title">' . $info['title'] . '</div>
        <div class="tabs-panel-content">' . $info['content'] . '</div>
      </div>
    ';
  }

  $output = theme('item_list', array(
    'items' => $tabs_title,
    'attributes' => array(
      'class' => array('tabs-nav'),
    ),
  ));
  $output .= implode("\n", $tabs_content);

  return '<div' . drupal_attributes($vars['attributes']) . '>' . $output . '</div>';
}

/**
 * Implements hook_html_head_alter().
 */
function base_html_head_alter(&$head_elements) {
  unset($head_elements['system_meta_generator']);

  foreach ($head_elements as $key => $element) {
    if (isset($element['#attributes']['rel']) && $element['#attributes']['rel'] == 'shortlink') {
      unset($head_elements[$key]);
    }
  }
}

/**
 * Implements hook_css_alter().
 */
function base_css_alter(&$css) {
  $removed_css = array(
    'modules/system/system.menus.css',
    'modules/system/system.theme.css',
    'misc/ui/jquery.ui.theme.css',
  );
  foreach ($removed_css as $path) {
    if (isset($css[$path])) {
      unset($css[$path]);
    }
  }
}

/**
 * Preprocess function for html.tpl.php.
 */
function base_preprocess_html(&$vars) {
  // Remove extra classes from body
  $vars['classes_array'] = array_diff($vars['classes_array'], array(
    'html',
    'one-sidebar sidebar-first',
    'one-sidebar sidebar-second',
  ));

  // Add sidebar classes in body
  if (!empty($vars['page']['sidebar_first']) && empty($vars['page']['sidebar_second'])) {
    $vars['classes_array'][] = 'one-sidebar-first';
  }
  elseif (empty($vars['page']['sidebar_first']) && !empty($vars['page']['sidebar_second'])) {
    $vars['classes_array'][] = 'one-sidebar-second';
  }

  // Add classes in body by current path
  if (theme_get_setting('base_body_classes_path')) {
    $request_path_array = explode('/', request_path());
    if ($request_path_array) {
      if ($request_path_array[0] == $GLOBALS['language']->language) {
        unset($request_path_array[0]);
      }
      $class = 'path';
      foreach ($request_path_array as $arg) {
        $class .= '-' . $arg;
        $vars['classes_array'][] = $class;
      }
    }
  }

  // Add classes in body by user roles
  if (theme_get_setting('base_body_classes_role')) {
    foreach ($GLOBALS['user']->roles as $key => $value) {
      $vars['classes_array'][] = 'user-role-' . $key;
    }
  }

  // Add classes in body by vocabulary name
  if (arg(0) == 'taxonomy' && arg(1) == 'term' && $term = taxonomy_term_load(arg(2))) {
    $vars['classes_array'][] = 'vocabulary-' . drupal_html_class($term->vocabulary_machine_name);
  }

  // Meta tags
  global $theme_info;
  if (!empty($theme_info->info['meta'])) {
    foreach ($theme_info->info['meta'] as $name => $content) {
      drupal_add_html_head(array(
        '#type' => 'html_tag',
        '#tag' => 'meta',
        '#attributes' => array(
          'name' => $name,
          'content' => $content,
        )
      ), 'meta_' . $name);
    }
  }

  // Performance statistics
  $stat = array(
    date('r'),
    timer_read('page') . ' ms',
    round(memory_get_usage() / 1024 / 1024, 2) . ' mb',
  );
  $vars['page']['page_bottom']['base_stat'] = array(
    '#markup' => '<!-- ' . implode(' | ', $stat) . ' -->',
  );
}

/**
 * Implements hook_page_alter().
 */
function base_page_alter(&$page) {
  // Rewrite region render logic
  foreach (element_children($page) as $region_name) {
    if ($page[$region_name] && isset($page[$region_name]['#region'])) {
      $page[$region_name]['#theme'] = 'region';
      unset($page[$region_name]['#theme_wrappers']);
    }
  }

  // Save before_comments region data
  if (isset($page['before_comments'])) {
    $GLOBALS['region_before_comments'] = $page['before_comments'];
  }

  // Hide content block on front
  if (theme_get_setting('base_hide_content_on_front') && drupal_is_front_page()) {
    $page['content']['system_main']['#access'] = FALSE;
  }

  // Attach OwlCarousel
  if (theme_get_setting('base_use_owlcarousel')) {
    $owlcarousel_paths = theme_get_setting('base_owlcarousel_paths');
    if (!$owlcarousel_paths
    || drupal_match_path(current_path(), $owlcarousel_paths)
    || drupal_match_path(request_path(), $owlcarousel_paths)) {
      drupal_add_library('base', 'owl.carousel', !$owlcarousel_paths);
    }
  }

  // Attach $.cookie
  drupal_add_library('system', 'jquery.cookie', TRUE);
}

/**
 * Preprocess function for node.tpl.php.
 */
function base_preprocess_node(&$vars) {
  // Add suggestion by view mode
  $node_type_suggestion_key = array_search('node__' . $vars['type'], $vars['theme_hook_suggestions']);
  if ($node_type_suggestion_key !== FALSE) {
    array_splice($vars['theme_hook_suggestions'], $node_type_suggestion_key + 1, 0, array('node__' . $vars['type'] . '__' . $vars['view_mode']));
  }

  // Remove extra classes from node wrapper
  $vars['classes_array'] = array_diff($vars['classes_array'], array(
    'node',
    'node-teaser',
    'node-promoted',
    'node-' . drupal_html_class($vars['type']),
  ));

  // Add classes to node wrapper
  $node_class_prefix = _base_get_entity_class_prefix('node', $vars['type']);
  $vars['classes_array'][] = $node_class_prefix;
  $vars['classes_array'][] = $node_class_prefix . '-' . drupal_html_class($vars['view_mode']);
  $vars['classes_array'][] = $node_class_prefix . '-' . $vars['nid'];

  // Add class to node title
  $vars['title_attributes_array']['class'][] = 'node-title';

  // Add data-nid attribute
  $vars['attributes_array']['data-nid'] = $vars['nid'];

  // Hide statistic
  if (isset($vars['content']['links']['statistics'])) {
    $vars['content']['links']['statistics']['#access'] = FALSE;
  }

  // Invoke node pseudo preprocess function
  _base_invoke_preudo_preprocess('preprocess_node__' . $vars['type'], $vars);
  _base_invoke_preudo_preprocess('preprocess_node__' . $vars['type'] . '__' . $vars['view_mode'], $vars);
}

/**
 * Preprocess function for field.tpl.php.
 */
function base_preprocess_field(&$vars) {
  static $class_cache = array(), $fields_cardinality = array(), $field_formatter_class_enabled;

  $element = &$vars['element'];
  $field_name = $element['#field_name'];

  // Add info about field cardinality
  if (!isset($fields_cardinality[$field_name])) {
    $field_info = field_info_field($field_name);
    $fields_cardinality[$field_name] = (int)$field_info['cardinality'];
  }
  $element['#cardinality'] = $fields_cardinality[$field_name];

  $vars['tag'] = ($element['#label_display'] == 'inline') ? 'span' : 'div';

  if ($vars['label'] && empty($element['#hide_label_colon'])) {
    $vars['label'] .= ':';
  }

  // Remove extra classes from field wrapper
  $vars['classes_array'] = array_diff($vars['classes_array'], array(
    'field',
    'field-name-' . $vars['field_name_css'],
    'field-type-' . $vars['field_type_css'],
    'field-label-hidden',
    'field-label-above',
    'field-label-inline',
    'clearfix',
  ));

  // Add classes to field content wrapper
  if ($field_formatter_class_enabled === NULL) {
    $field_formatter_class_enabled = module_exists('field_formatter_class');
  }
  if (!$field_formatter_class_enabled) {
    $class_cache_key = $element['#entity_type'] . ':' . $element['#bundle'] . ':' . $field_name;

    if (!isset($class_cache[$class_cache_key])) {
      $entity_class_prefix = _base_get_entity_class_prefix($element['#entity_type'], $element['#bundle']);
      $field_class = str_replace(array('field-' . $entity_class_prefix . '-', 'field-', '-field', 'commerce-'), '', $vars['field_name_css']);
      $class_cache[$class_cache_key] = $entity_class_prefix . '-' . $field_class;
    }

    $vars['field_name_css'] = $class_cache[$class_cache_key];
    array_unshift($vars['classes_array'], $vars['field_name_css']);
  }

  // Add class to list_integer field
  if ($element['#field_type'] == 'list_integer' && $element['#cardinality'] == 1) {
    $vars['classes_array'][] = $vars['field_name_css'] . '-' . $element['#items'][0]['value'];
  }

  // Invoke field pseudo preprocess function
  _base_invoke_preudo_preprocess('preprocess_field__' . $field_name, $vars);
}

/**
 * Override theme_field().
 */
function base_field($vars) {
  $output = '';
  $tag = $vars['tag'];
  $wrapper_classes = $vars['classes'];
  $wrapper_attributes = $vars['attributes'];
  $label = $vars['label'];
  $label_attributes = $vars['title_attributes'];
  $items_attributes = $vars['content_attributes'];

  // Label
  if (!$vars['label_hidden']) {
    $output .= "<$tag class=\"field-label\"$label_attributes>$label</$tag>\n";
  }

  // Items
  $items = '';
  if ($vars['element']['#cardinality'] == 1 || !empty($vars['element']['#no_item_wrapper'])) {
    $items = drupal_render($vars['items']);
  }
  else {
    foreach ($vars['items'] as $item) {
      if ($item_markup = drupal_render($item)) {
        $items .= "<$tag class=\"field-item\">" . $item_markup . "</$tag>\n";
      }
    }
  }
  if (!$vars['label_hidden'] || $items_attributes) {
    $output .= "<$tag class=\"field-value\"$items_attributes>$items</$tag>\n";
  }
  else {
    $output .= $items;
  }

  // Wrapper
  if ($wrapper_classes || $wrapper_attributes) {
    $output = "<div class=\"$wrapper_classes\"$wrapper_attributes>$output</div>\n";
  }

  return $output;
}

/**
 * Implements hook_field_attach_view_alter().
 */
function base_field_attach_view_alter(&$output, $context) {
  // Remove field collection wrapper
  foreach (element_children($output) as $key) {
    if ($output[$key]['#field_type'] == 'field_collection') {
      $output[$key]['#prefix'] = '';
      $output[$key]['#suffix'] = '';
    }
  }
}

/**
 * Preprocess function for views-view.tpl.php.
 */
function base_preprocess_views_view(&$vars) {
  // Remove extra classes
  $remove_classes = array('view-id-' . $vars['name'], 'view-display-id-' . $vars['display_id']);
  if (!$vars['view']->use_ajax && isset($vars['dom_id'])) {
    $remove_classes[] = 'view-dom-id-' . $vars['dom_id'];
  }
  $vars['classes_array'] = array_diff($vars['classes_array'], $remove_classes);

  // Add class
  $vars['classes_array'][] = 'view-' . drupal_html_class($vars['name'] . '-' . $vars['display_id']);

  // Settings from css
  $vars['css_settings'] = array();
  if ($vars['css_class']) {
    $css_classes_array = explode(' ', $vars['css_class']);

    foreach ($css_classes_array as $key => $class) {
      if (strpos($class, 'setting-') === 0) {
        $vars['css_settings'][] = $class;
        unset($css_classes_array[$key]);
      }
    }

    $css_class_key = array_search($vars['css_class'], $vars['classes_array']);
    unset($vars['classes_array'][$css_class_key]);
    $vars['classes_array'] = array_merge($vars['classes_array'], $css_classes_array);
  }
}

/**
 * Process function for views-view.tpl.php.
 */
function base_process_views_view(&$vars) {
  $vars['view_element_start'] = '<div class="' . $vars['classes'] . '">';
  $vars['view_element_end'] = '</div>';
  $vars['view_content_element_start'] = '<div class="view-content">';
  $vars['view_content_element_end'] = '</div>';

  if (in_array('setting-no-view-wrapper', $vars['css_settings'])) {
    $vars['view_element_start'] = '';
    $vars['view_element_end'] = '';
  }
  if (in_array('setting-no-content-wrapper', $vars['css_settings'])) {
    $vars['view_content_element_start'] = '';
    $vars['view_content_element_end'] = '';
  }
  if (in_array('setting-add-content-inner', $vars['css_settings'])) {
    $vars['view_content_element_start'] .= '<div class="view-content-inner">';
    $vars['view_content_element_end'] = '</div>' . $vars['view_content_element_end'];
  }
}

/**
 * Preprocess function for block.tpl.php.
 */
function base_preprocess_block(&$vars) {
  $block = $vars['block'];

  // Change block id
  if ($block->module == 'menu') {
    $vars['block_html_id'] = 'block-' . $block->delta;
    if (strpos($block->delta, 'menu') === FALSE) {
      $vars['block_html_id'] .= '-menu';
    }
  }
  elseif ($block->module == 'views') {
    $delta = str_replace('-block', '', $block->delta);
    $delta = ltrim($delta, '-');
    $vars['block_html_id'] = 'block-' . drupal_html_class($delta);
  }
  elseif ($block->module == 'block') {
    $vars['block_html_id'] = 'block-custom-' . $block->delta;
  }
  elseif ($block->module == 'webform') {
    $vars['block_html_id'] = 'block-webform-' . str_replace('client-block-', '', $block->delta);
  }
  elseif ($block->module == 'search') {
    if ($block->delta == 'form') {
      $vars['block_html_id'] = 'block-search';
    }
  }
  else {
    $vars['block_html_id'] = drupal_html_id('block-' . $block->delta);
  }

  // Remove extra class
  $vars['classes_array'] = array_diff($vars['classes_array'], array(
    drupal_html_class('block-' . $block->module),
    'block-menu',
  ));

  // Add classes
  if ($block->module == 'commerce_cart') {
    if ($block->delta == 'cart' && strpos($vars['content'], 'cart-empty-block') !== FALSE) {
      $vars['classes_array'][] = 'block-cart-empty';
    }
  }

  // Add class to block title
  $vars['title_attributes_array']['class'][] = 'block-title';

  // Allow html title
  if ($block->title != '<none>' && strpos($block->title, '<') !== FALSE) {
    $block->subject = filter_xss($block->title, array('span', 'div', 'a'));
  }

  // Clean blocks
  if ($block->module == 'xblockify') {
    $vars['theme_hook_suggestions'][] = 'block__without_wrappers';
  }

  // Build time
  if (theme_get_setting('base_show_timers') && isset($block->build_time)) {
    $build_time = intval($block->build_time * 100000) / 100;
    $vars['content'] .= "\n<!-- Block build time: $build_time ms -->\n";
  }
}

/**
 * Preprocess function for comment-wrapper.tpl.php.
 */
function base_preprocess_comment_wrapper(&$vars) {
  $vars['classes_array'] = array_diff($vars['classes_array'], array('comment-wrapper'));
  $vars['classes_array'][] = 'comments-wrapper';
}

/**
 * Preprocess function for comment.tpl.php.
 */
function base_preprocess_comment(&$vars) {
  $vars['title_attributes_array']['class'][] = 'comment-title';
}

/**
 * Preprocess function for theme_html_tag().
 */
function base_preprocess_html_tag(&$vars) {
  $element = &$vars['element'];

  // Cleaning scripts
  if ($element['#tag'] == 'script') {
    if (isset($element['#attributes']['type']) && $element['#attributes']['type'] == 'text/javascript') {
      unset($element['#attributes']['type']);
    }

    if (isset($element['#value_prefix']) && $element['#value_prefix'] == "\n<!--//--><![CDATA[//><!--\n") {
      $element['#value_prefix'] = "\n";
      $element['#value_suffix'] = "\n";
    }

    // Remove base root
    if (isset($element['#attributes']['src'])) {
      $element['#attributes']['src'] = preg_replace('#^' . preg_quote($GLOBALS['base_root']) . '#', '', $element['#attributes']['src']);
    }
  }
  // Cleaning styles
  elseif ($element['#tag'] == 'style') {
    if ($element['#attributes']['type'] == 'text/css') {
      unset($element['#attributes']['type']);
    }

    $element['#value_prefix'] = "\n";
    $element['#value_suffix'] = "\n";

    // Remove base root
    $element['#value'] = str_replace('url("' . $GLOBALS['base_root'], 'url("', $element['#value']);
  }
  // Cleaning head links
  elseif ($element['#tag'] == 'link') {
    if ($element['#attributes']['rel'] == 'stylesheet') {
      $element['#attributes']['href'] = str_replace($GLOBALS['base_root'], '', $element['#attributes']['href']);
    }
  }
}

/**
 * Preprocess function for theme_captcha().
 */
function base_preprocess_captcha(&$vars) {
  if ($vars['element']['#captcha_type'] == 'image_captcha/Image' && isset($vars['element']['captcha_widgets'])) {
    $vars['element']['captcha_widgets']['captcha_response']['#field_prefix'] = drupal_render($vars['element']['captcha_widgets']['captcha_image']);
    $vars['element']['captcha_widgets']['captcha_image']['#access'] = FALSE;
  }
}

/**
 * Preprocess function for theme_image().
 */
function base_preprocess_image(&$vars) {
  // Remove empty attributes
  foreach (array('title') as $key) {
    if (isset($vars[$key]) && $vars[$key] === '') {
      unset($vars[$key]);
    }
  }
}

/**
 * Preprocess fucntion for theme_menu_tree().
 */
function base_preprocess_menu_tree(&$vars) {
  $first_item_key = current(element_children($vars['#tree']));
  $first_item = $vars['#tree'][$first_item_key];
  $vars['menu_name'] = $first_item['#original_link']['menu_name'];
  $vars['menu_depth'] = $first_item['#original_link']['depth'];
  $vars['menu_items_count'] = count(element_children($vars['#tree']));

  $vars['classes_array'] = array(
    'menu',
    'menu-level-' . $vars['menu_depth'],
    'menu-items-' . $vars['menu_items_count'],
  );
}

/**
 * Override theme_menu_tree().
 */
function base_menu_tree($vars) {
  return '<ul class="' . implode(' ', $vars['classes_array']) . '">' . $vars['tree'] . '</ul>';
}

/**
 * Preprocess function for theme_menu_link().
 */
function base_preprocess_menu_link(&$vars) {
  $element = &$vars['element'];

  // Remove extra classes
  $element['#attributes']['class'] = array_diff($element['#attributes']['class'], array(
    'first',
    'last',
    'leaf',
    'menu-mlid-' . $element['#original_link']['mlid'],
  ));

  // Add class by mlid
  array_unshift($element['#attributes']['class'], 'menu-item', 'menu-item-' . $element['#original_link']['mlid']);

  // Remove empty title
  if (
    isset($element['#localized_options']['attributes']['title']) &&
    (
      !$element['#localized_options']['attributes']['title'] ||
      $element['#localized_options']['attributes']['title'] == $element['#title']
    )
  ) {
    unset($element['#localized_options']['attributes']['title']);
  }

  // Invoke pseudo preprocess function
  $menu_name = str_replace('-', '_', $element['#original_link']['menu_name']);
  _base_invoke_preudo_preprocess('preprocess_menu_link__' . $menu_name, $vars);
}

/**
 * Preprocess function for base_region().
 */
function base_preprocess_region(&$vars) {
  static $suggestions;

  $vars['content'] = array();
  foreach (element_children($vars['elements']) as $block_key) {
    $vars['content'][$block_key] = $vars['elements'][$block_key];
  }

  if ($suggestions === NULL) {
    $suggestions = theme_get_suggestions(arg(), '');
  }
  if ($suggestions) {
    foreach ($suggestions as $suggestion) {
      $vars['theme_hook_suggestions'][] = 'region__' . $vars['region'] . $suggestion;
    }
  }
}

/**
 * Override region.tpl.php.
 */
function base_region($vars) {
  return render($vars['content']);
}

/**
 * Preprocess function for theme_breadcrumb().
 */
function base_preprocess_breadcrumb(&$vars) {
  // Add current page in breadcrumb
  if (!empty($vars['breadcrumb']) && theme_get_setting('base_current_page_in_breadcrumb')) {
    $page_title = drupal_get_title();
    $last_breadcrumb_item = end($vars['breadcrumb']);
    if ($last_breadcrumb_item != $page_title && strpos($last_breadcrumb_item, '<a ') !== FALSE) {
      $vars['breadcrumb'][] = $page_title;
    }
  }
}

/**
 * Override theme_breadcrumb().
 */
function base_breadcrumb($vars) {
  if (!empty($vars['breadcrumb'])) {
    if (count($vars['breadcrumb']) == 1 && theme_get_setting('base_hide_one_breadcrumb')) {
      return;
    }

    $output = '';
    foreach ($vars['breadcrumb'] as $breadcrumb) {
      $output .= '<li class="breadcrumb-item">' . $breadcrumb . '</li>' . "\n";
    }
    return '<ul class="breadcrumbs">' . $output . '</ul>';
  }
}

/**
 * Override theme_textarea().
 */
function base_textarea($vars) {
  $element = $vars['element'];
  element_set_attributes($element, array('id', 'name', 'cols', 'rows'));
  _form_set_class($element, array('form-textarea'));
  return '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
}

/**
 * Override theme_item_list().
 */
function base_item_list($vars) {
  $items = $vars['items'];
  $title = $vars['title'];
  $type = $vars['type'];
  $attributes = $vars['attributes'];
  $output = '';

  if (isset($title) && $title !== '') {
    $output .= '<h3>' . $title . '</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    $i = 0;
    foreach ($items as $item) {
      $attributes = array();
      $children = array();
      $data = '';
      $i++;
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        $data .= theme('item_list', array(
          'items' => $children,
          'title' => NULL,
          'type' => $type,
          'attributes' => $attributes,
        ));
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= "</$type>";
  }

  return $output;
}

/**
 * Override theme_links().
 */
function base_links($variables) {
  $links = $variables['links'];
  $attributes = $variables['attributes'];
  $heading = $variables['heading'];
  global $language_url;
  $output = '';

  if (count($links) > 0) {
    // Treat the heading first if it is present to prepend it to the
    // list of links.
    if (!empty($heading)) {
      if (is_string($heading)) {
        // Prepare the array that will be used when the passed heading
        // is a string.
        $heading = array(
          'text' => $heading,
          // Set the default level of the heading.
          'level' => 'h2',
        );
      }
      $output .= '<' . $heading['level'];
      if (!empty($heading['class'])) {
        $output .= drupal_attributes(array('class' => $heading['class']));
      }
      $output .= '>' . check_plain($heading['text']) . '</' . $heading['level'] . '>';
    }

    $output .= '<ul' . drupal_attributes($attributes) . '>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = array($key);
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
          && (empty($link['language']) || $link['language']->language == $language_url->language)) {
        $class[] = 'active';
      }
      $output .= '<li' . drupal_attributes(array('class' => $class)) . '>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      elseif (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes.
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}

/**
 * Override theme_link().
 */
function base_link($variables) {
  $text = $variables['options']['html'] ? $variables['text'] : check_plain($variables['text']);
  if ($variables['path'] != '<none>') {
    $href = check_plain(url($variables['path'], $variables['options']));
    return '<a href="' . $href . '"' . drupal_attributes($variables['options']['attributes']) . '>' . $text . '</a>';
  }
  else {
    return '<span' . drupal_attributes($variables['options']['attributes']) . '>' . $text . '</span>';
  }
}

/**
 * Override theme_menu_local_tasks().
 */
function base_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<ul class="primary-tabs">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<ul class="secondary-tabs">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

/**
 * Preprocess function for theme_pager().
 */
function base_preprocess_pager(&$vars) {
  if ($vars['quantity'] > 5) {
    $vars['quantity'] = 5;
  }
}

/**
 * Override theme_pager().
 */
function base_pager($variables) {
  global $pager_page_array, $pager_total;

  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  $pager_middle = ceil($quantity / 2);
  $pager_current = $pager_page_array[$element] + 1;
  $pager_first = $pager_current - $pager_middle + 1;
  $pager_last = $pager_current + $quantity - $pager_middle;
  $pager_max = $pager_total[$element];

  $i = $pager_first;
  if ($pager_last > $pager_max) {
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }

  $li_first = theme('pager_first', array(
    'text' => (isset($tags[0]) ? $tags[0] : t('« first')),
    'element' => $element,
    'parameters' => $parameters
  ));
  $li_previous = theme('pager_previous', array(
    'text' => (isset($tags[1]) ? $tags[1] : t('‹ previous')),
    'element' => $element, 'interval' => 1,
    'parameters' => $parameters
  ));
  $li_next = theme('pager_next', array(
    'text' => (isset($tags[3]) ? $tags[3] : t('next ›')),
    'element' => $element,
    'interval' => 1,
    'parameters' => $parameters
  ));
  $li_last = theme('pager_last', array(
    'text' => (isset($tags[4]) ? $tags[4] : t('last »')),
    'element' => $element,
    'parameters' => $parameters
  ));

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => array('pager-first'),
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-current'),
            'data' => $i,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'),
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => array('pager-last'),
        'data' => $li_last,
      );
    }

    return theme('item_list', array(
      'items' => $items,
      'attributes' => array(
        'class' => array('pager'),
      ),
    ));
  }
}

/**
 * Implements hook_element_info_alter().
 */
function base_element_info_alter(&$type) {
  if (isset($type['text_format'])) {
    $type['text_format']['#process'][] = 'base_text_format_process';
  }
}

/**
 * Process callback for element "text_format".
 */
function base_text_format_process($element) {
  $element['format']['help']['#access'] = FALSE;
  return $element;
}

/**
 * Implements hook_form_FORM_ID_alter(): search_block_form.
 */
function base_form_search_block_form_alter(&$form, &$form_state) {
  if (theme_get_setting('base_search_block_placeholder')) {
    $form['search_block_form']['#attributes']['placeholder'] = t(theme_get_setting('base_search_block_placeholder'));
  }
}

/**
 * Preprocess function for theme_form_element_label().
 */
function base_preprocess_form_element_label(&$vars) {
  static $add_pseudo_input;
  if ($add_pseudo_input === NULL) {
    $add_pseudo_input = theme_get_setting('base_add_pseudo_input');
  }

  if ($add_pseudo_input && isset($vars['element']['#type'])) {
    if ($vars['element']['#type'] == 'checkbox' || $vars['element']['#type'] == 'radio') {
      $vars['element']['#title'] = '<span class="pseudo-' . $vars['element']['#type'] . '"></span>' . $vars['element']['#title'];
    }
  }
}

/**
 * Implements hook_block_list_alter().
 */
function base_block_list_alter(&$blocks) {
  $GLOBALS['block_build_microtime'] = microtime(TRUE);
}

/**
 * Implements hook_block_view_alter().
 */
function base_block_view_alter(&$data, $block) {
  if (!isset($GLOBALS['block_build_microtime'])) {
    $GLOBALS['block_build_microtime'] = microtime(TRUE);
  }
  $block->build_time = microtime(TRUE) - $GLOBALS['block_build_microtime'];
  $GLOBALS['block_build_microtime'] = microtime(TRUE);
}

/**
 * Button theme function.
 */
function base_button_button($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name'));

  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }

  if (!isset($element['#attributes']['value'])) {
    $element['#attributes']['value'] = $element['#value'];
  }

  return '<button' . drupal_attributes($element['#attributes']) . '>' . $element['#value'] . '</button>';
}

/**
 * Invoke pseudo preprocess function.
 */
function _base_invoke_preudo_preprocess($hook, &$vars) {
  $pseudo_preprocess_function = $GLOBALS['theme'] . '_' . $hook;
  if (function_exists($pseudo_preprocess_function)) {
    $pseudo_preprocess_function($vars);
  }
}

/**
 * Return entity class prefix.
 */
function _base_get_entity_class_prefix($entity_type, $bundle_name) {
  global $theme_info;

  if (!empty($theme_info->info['entity_class_prefix'][$entity_type][$bundle_name])) {
    return $theme_info->info['entity_class_prefix'][$entity_type][$bundle_name];
  }

  return drupal_html_class($bundle_name);
}
