<?php

/**
 * Implements hook_form_FORM_ID_alter(): system_theme_settings.
 */
function base_form_system_theme_settings_alter(&$form, $form_state) {
  $form['advanced_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
  );

  $form['advanced_settings']['base_hide_content_on_front'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide content block on front'),
    '#default_value' => theme_get_setting('base_hide_content_on_front'),
  );

  $form['advanced_settings']['base_body_classes_path'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add css classes in body by path'),
    '#default_value' => theme_get_setting('base_body_classes_path'),
  );

  $form['advanced_settings']['base_body_classes_role'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add css classes in body by user role'),
    '#default_value' => theme_get_setting('base_body_classes_role'),
  );

  $form['advanced_settings']['base_current_page_in_breadcrumb'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show current page in breadcrumb'),
    '#default_value' => theme_get_setting('base_current_page_in_breadcrumb'),
  );

  $form['advanced_settings']['base_hide_one_breadcrumb'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide breadcrumb with one item'),
    '#default_value' => theme_get_setting('base_hide_one_breadcrumb'),
  );

  $form['advanced_settings']['base_add_pseudo_input'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add pseudo input in checkbox and radio labels'),
    '#default_value' => theme_get_setting('base_add_pseudo_input'),
  );

  $form['advanced_settings']['base_show_timers'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show timers'),
    '#default_value' => theme_get_setting('base_show_timers'),
  );

  $form['advanced_settings']['base_commerce_pretty_classes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Commerce pretty classes'),
    '#default_value' => theme_get_setting('base_commerce_pretty_classes'),
  );

  $form['advanced_settings']['base_use_owlcarousel'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Owl Carousel'),
    '#default_value' => theme_get_setting('base_use_owlcarousel'),
  );

  $form['advanced_settings']['base_owlcarousel_paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Owl Carousel paths'),
    '#description' => t('Leave empty if attach Owl Carousel on all pages'),
    '#default_value' => theme_get_setting('base_owlcarousel_paths'),
    '#states' => array(
      'visible' => array(
        'input[name="base_use_owlcarousel"]' => array(
          'checked' => TRUE,
        ),
      ),
    ),
  );

  $form['advanced_settings']['base_search_block_placeholder'] = array(
    '#type' => 'textfield',
    '#title' => t('Search block input placeholder'),
    '#default_value' => theme_get_setting('base_search_block_placeholder'),
  );
}
