<?php

/**
 * @file
 * Default theme implementation to present the SKU on a product page.
 *
 * Available variables:
 * - $sku: The SKU to render.
 * - $label: If present, the string to use as the SKU label.
 *
 * Helper variables:
 * - $product: The fully loaded product object the SKU represents.
 */
?>
<?php if ($sku) { ?>
  <div class="product-sku">
    <?php if ($label) { ?>
      <div class="field-label"><?php echo $label; ?></div>
    <?php } ?>
    <div class="field-value"><?php echo $sku; ?></div>
  </div>
<?php } ?>
