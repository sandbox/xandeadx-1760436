<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */

echo $wrapper_prefix;
if (!empty($title)) {
  echo "<h3>$title</h3>\n";
}
echo $list_type_prefix;
foreach ($rows as $id => $row) {
  $class = !empty($classes_array[$id]) ? ' class="' . $classes_array[$id] . '"' : '';
  echo "<li$class>$row</li>\n";
}
echo $list_type_suffix;
echo $wrapper_suffix;
