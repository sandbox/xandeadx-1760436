<div id="<?php echo $block_html_id; ?>" class="<?php echo $classes; ?>"<?php echo $attributes; ?>>
  <?php echo render($title_prefix); ?>
  <?php if ($block->subject) { ?>
    <h3<?php echo $title_attributes; ?>><?php echo $block->subject; ?></h3>
  <?php } ?>
  <?php echo render($title_suffix); ?>

  <div class="block-content"<?php echo $content_attributes; ?>>
    <?php echo $content; ?>
  </div>
</div>
