<div class="page">
  <?php if ($page['header']) { ?>
    <header class="page-header">
      <div class="page-header-inner">
        <?php echo render($page['header']); ?>
      </div>
    </header>
  <?php } ?>

  <div class="page-main-wrapper">
    <?php if ($page['main_header']) { ?>
      <div class="page-main-header">
        <div class="page-main-header-inner">
          <?php echo render($page['main_header']); ?>
        </div>
      </div>
    <?php } ?>

    <div class="page-main">
      <div class="page-main-inner">

        <div class="page-content">
          <div class="page-content-inner">
            <?php if ($page['help']) { ?>
              <div class="page-help">
                <?php echo render($page['help']); ?>
              </div>
            <?php } ?>

            <?php if ($page['content_header']) { ?>
              <div class="content-header">
                <?php echo render($page['content_header']); ?>
              </div>
            <?php } ?>

            <div class="content-main">
              <?php echo render($page['content']); ?>
            </div>

            <?php if ($page['content_footer']) { ?>
              <div class="content-footer">
                <?php echo render($page['content_footer']); ?>
              </div>
            <?php } ?>
          </div>
        </div>

        <?php if ($page['sidebar_first']) { ?>
          <aside class="sidebar-first">
            <?php echo render($page['sidebar_first']); ?>
          </aside>
        <?php } ?>

        <?php if ($page['sidebar_second']) { ?>
          <aside class="sidebar-second">
            <?php echo render($page['sidebar_second']); ?>
          </aside>
        <?php } ?>

      </div>
    </div>

    <?php if ($page['main_footer']) { ?>
      <div class="page-main-footer">
        <div class="page-main-footer-inner">
          <?php echo render($page['main_footer']); ?>
        </div>
      </div>
    <?php } ?>
  </div>

  <?php if ($page['after_main']) { ?>
    <div class="page-after-main">
      <div class="page-after-main-inner">
        <?php echo render($page['after_main']); ?>
      </div>
    </div>
  <?php } ?>
</div>

<?php if ($page['footer']) { ?>
  <footer class="page-footer">
    <div class="page-footer-inner">
      <?php echo render($page['footer']); ?>
    </div>
  </footer>
<?php } ?>

<?php if ($page['bottom']) { ?>
  <div class="page-bottom">
    <div class="page-bottom-inner">
      <?php echo render($page['bottom']); ?>
    </div>
  </div>
<?php } ?>
