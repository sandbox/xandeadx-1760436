<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 *
 * @ingroup themeable
 */
?>
<!DOCTYPE html>
<html>
<head>
  <title><?php echo $head_title; ?></title>
  <?php echo $head; ?>
</head>
<body class="<?php echo $classes; ?>">
  <?php echo $content; ?>
</body>
</html>
