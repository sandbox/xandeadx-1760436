<div<?php echo drupal_attributes($form['#attributes']); ?> id="<?php echo $form['#id']; ?>">
  <?php if (!empty($form['#title'])) { ?>
    <div class="checkout-pane-title"><?php echo $form['#title']; ?></div>
  <?php } ?>
  <div class="checkout-pane-content">
    <?php echo drupal_render_children($form); ?>
  </div>
</div>
