<?php if ($widget->widget) { ?>
  <div class="views-widget views-widget-<?php echo drupal_html_class($widget->id); ?>">
    <?php if (!empty($widget->label)) { ?>
      <label for="<?php echo $widget->id; ?>">
        <?php echo $widget->label; ?>
      </label>
    <?php } ?>

    <?php if (!empty($widget->operator)) { ?>
      <div class="widget-operator">
        <?php echo $widget->operator; ?>
      </div>
    <?php } ?>

    <?php echo $widget->widget; ?>

    <?php if (!empty($widget->description)) { ?>
      <div class="widget-description">
        <?php echo $widget->description; ?>
      </div>
    <?php } ?>
  </div>
<?php } ?>
