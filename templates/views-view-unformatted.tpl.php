<?php if (!empty($title)) { ?>
  <h3><?php print $title; ?></h3>
<?php } ?>

<?php foreach ($rows as $id => $row) { ?>
  <?php if ($classes_array[$id]) { ?>
    <div<?php if ($classes_array[$id]) { echo ' class="' . $classes_array[$id] .'"';  } ?>>
      <?php echo $row; ?>
    </div>
  <?php } else { ?>
    <?php echo $row; ?>
  <?php } ?>
<?php } ?>
