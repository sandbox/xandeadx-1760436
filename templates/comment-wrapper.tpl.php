<div class="<?php echo $classes; ?>"<?php echo $attributes; ?>>
  <?php if ($content['comments'] && $node->type != 'forum') { ?>
    <?php echo render($title_prefix); ?>
    <h3 class="comments-title"><?php echo t('Comments'); ?></h3>
    <?php echo render($title_suffix); ?>
  <?php } ?>

  <div class="comments">
    <?php echo render($content['comments']); ?>
  </div>

  <?php if ($content['comment_form']) { ?>
    <h3 class="comment-form-title"><?php echo t('Add new comment'); ?></h3>
    <?php echo render($content['comment_form']); ?>
  <?php } ?>
</div>
