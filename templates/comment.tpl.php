<div class="<?php echo $classes; ?>"<?php echo $attributes; ?>>
  <?php echo $picture; ?>

  <?php echo render($title_prefix); ?>
  <?php if ($title) { ?>
    <div<?php echo $title_attributes; ?>><?php echo $title; ?></div>
  <?php } ?>
  <?php echo render($title_suffix); ?>

  <div class="comment-submitted">
    <?php echo $permalink; ?>
    <?php echo $submitted; ?>
  </div>

  <?php
  hide($content['links']);
  echo render($content);
  ?>

  <?php if ($signature) { ?>
    <div class="user-signature"><?php echo $signature; ?></div>
  <?php } ?>

  <?php echo render($content['links']); ?>
</div>
