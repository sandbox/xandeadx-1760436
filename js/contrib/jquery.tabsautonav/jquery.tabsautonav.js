(function($) {
  if (!$.ui.tabs) {
    return;
  }

  $.widget('ui.tabs', $.ui.tabs, {
    /**
     * Override _create().
     */
    _create: function() {
      var options = $.extend({
        createNav: false,
        titleSelector: 'h2',
        removeTitle: true,
        removeEmpty: false
      }, this.options);

      if (options.createNav) {
        var tabsNav = '';

        this.element.find('> *').each(function() {
          var $panel = $(this);
          var $title = $panel.find(options.titleSelector);

          if (options.removeEmpty && $panel.children().not($title).length == 0) {
            return;
          }

          tabsNav += '<li><a href="#' + $panel.attr('id') + '">' + $title.html() + '</a></li>' + "\n";

          if (options.removeTitle) {
            $title.remove();
          }
        });

        $('<ul>' + tabsNav + '</ul>').prependTo(this.element);
      }

      this._superApply(arguments);
    }
  });
}(jQuery));
