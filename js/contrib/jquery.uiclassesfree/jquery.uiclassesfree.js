(function($) {
  var uiClasses = [
    'ui-state-active',
    'ui-state-default',
    'ui-helper-reset',
    'ui-helper-clearfix',
    'ui-widget',
    'ui-widget-header',
    'ui-widget-content',
    'ui-corner-all',
    'ui-corner-top',
    'ui-corner-right',
    'ui-corner-bottom',
    'ui-corner-left',
    'ui-corner-tr',
    'ui-corner-br',
    'ui-button',
    'ui-button-text-only'
  ];
  var selector = '.' + uiClasses.join(', .');

  $.fn.uiClassesFree = function() {
    this.find(selector).andSelf().removeClass(uiClasses.join(' '));
    return this;
  };
}(jQuery));
