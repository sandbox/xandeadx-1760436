(function($) {
  $.fn.dropdownlist = function (options) {
    var $list = this;
    if ($list.hasClass('dropdownlist-processed')) {
      return;
    }

    options = $.extend({
      activeSelector: 'li.active',
      copyClasses: false,
      select: null
    }, options);

    // Return item html
    var getItemHtml = function (item) {
      var $item = $(item);
      var $itemLink = $item.find('a');

      if ($itemLink.length) {
        return $itemLink.html();
      }
      else {
        return $item.html();
      }
    };

    var $activeItem = $list.find(options.activeSelector);

    var $trigger = $list.prev();
    if (!$trigger.hasClass('dropdownlist-trigger')) {
      $trigger = $('<a href="#" class="dropdownlist-trigger">' + getItemHtml($activeItem) + '</a>');
      $trigger.insertBefore($list);

      // Copy classes from active item to switcher
      if (options.copyClasses) {
        $trigger.addClass($activeItem.attr('class'));
      }
    }

    // Show list to click on visible link
    $trigger.click(function (event) {
      event.preventDefault();
      $list.toggleClass('dropdownlist-open');
    });

    // List item click handler
    $list.find('li').click(function (event) {
      $trigger.html(getItemHtml(this));

      $list.removeClass('dropdownlist-open');

      if (options.select) {
        options.select(event);
      }
    });

    // Add system classes
    if (!$list.hasClass('dropdownlist')) {
      $list.addClass('dropdownlist');
    }
    $list.addClass('dropdownlist-processed');
  };
}(jQuery));
