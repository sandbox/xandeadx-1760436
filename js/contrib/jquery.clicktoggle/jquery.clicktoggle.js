(function($) {
  var documentClickAttached = false;
  var autoCloseElements = [];

  $.fn.clickToggle = function(options) {
    return $(this).each(function() {
      var $clickedElement = $(this);
      var $targetElement;

      if ($clickedElement.data('clickToggleAttached')) {
        return this;
      }

      options = $.extend({
        target: $clickedElement.next(),
        targetClass: 'opened',
        targetFocusElement: null,
        clickedClass: '',
        bodyClass: '',
        focusElement: null,
        autoClose: false,
        openCallback: null,
        closeCallback: null
      }, options);

      if (options.target == 'parent') {
        $targetElement = $clickedElement.parent();
      }
      else if ($.isFunction(options.target)) {
        $targetElement = options.target($clickedElement, options);
      }
      else {
        $targetElement = $(options.target);
      }

      // Clicked element click handler
      $clickedElement.on('click', function(event, stopPropagation) {
        event.preventDefault();

        // Open
        if (!$targetElement.hasClass(options.targetClass)) {
          $targetElement.addClass(options.targetClass);

          if (options.clickedClass) {
            $clickedElement.addClass(options.clickedClass);
          }
          if (options.bodyClass) {
            $('body').addClass(options.bodyClass);
          }

          if (options.targetFocusElement) {
            $targetElement.find(options.targetFocusElement).focus();
          }
          if (options.openCallback) {
            options.openCallback($clickedElement, $targetElement);
          }
        }
        // Close
        else {
          $targetElement.removeClass(options.targetClass);

          if (options.clickedClass) {
            $clickedElement.removeClass(options.clickedClass);
          }
          if (options.bodyClass) {
            $('body').removeClass(options.bodyClass);
          }

          if (options.closeCallback) {
            options.closeCallback();
          }
        }

        if (stopPropagation) {
          event.stopPropagation();
        }
      });

      // Document click handler
      if (options.autoClose && !documentClickAttached) {
        $(document).on('click', function(event) {
          var $eventTarget = $(event.target);
          var $clickedElement;
          var $targetElement;
          var targetClass;

          for (var i = 0; i < autoCloseElements.length; i++) {
            $clickedElement = autoCloseElements[i].clickedElement;
            $targetElement = autoCloseElements[i].targetElement;
            targetClass = autoCloseElements[i].targetClass;

            if ($targetElement.hasClass(targetClass)
              && $eventTarget.closest($clickedElement).length == 0
              && $eventTarget.closest($targetElement).length == 0) {
              $clickedElement.trigger('click', [true]);
            }
          }
        });

        documentClickAttached = true;
      }

      if (options.autoClose) {
        autoCloseElements.push({
          clickedElement: $clickedElement,
          targetElement: $targetElement,
          targetClass: options.targetClass
        });
      }

      $clickedElement.data('clickToggleAttached', true);
    });
  };
}(jQuery));
