(function ($) {
  Drupal.behaviors.baseOwlCarousel = {
    attach: function (context, settings) {
      if (!$.fn.owlCarousel) {
        return;
      }

      Drupal.baseOwlCarousel.init(context);
    }
  };

  Drupal.baseOwlCarousel = {
    init: function (context) {
      $('.owl-carousel:not(.owl-loaded)', context).each(function () {
        var $owlCarousel = $(this);
        var $owlContent = $owlCarousel;
        var owlCarouselPreset = null;

        if ($owlCarousel.children('.view-content').length == 1) {
          $owlContent = $owlCarousel.children('.view-content');
        }
        else if ($owlCarousel.children('.owl-carousel-inner').length == 1) {
          $owlContent = $owlCarousel.children('.owl-carousel-inner');
        }

        if ($owlContent.children().length <= 1) {
          return false;
        }

        // Get preset name
        $.each($owlCarousel.attr('class').split(' '), function (index, className) {
          var presetMatches = className.match(/^owl-carousel-preset-(.+)$/);
          if (presetMatches) {
            owlCarouselPreset = presetMatches[1];
            return false;
          }
        });

        // Error handlers
        if (!owlCarouselPreset) {
          console.error('OwlCarousel preset name not found.');
          return false;
        }
        if (!Drupal.owlCarouselPresets) {
          console.error('OwlCarousel presets not exist.');
          return false;
        }
        if (!Drupal.owlCarouselPresets[owlCarouselPreset]) {
          console.error('OwlCarousel preset "' + owlCarouselPreset + '" not exist.');
          return false;
        }

        var options = Drupal.owlCarouselPresets[owlCarouselPreset];

        // Calculate margin
        if (typeof options.margin == 'string') {
          options.margin = parseInt($owlCarousel.children(':nth-child(2)').css('margin-' + options.margin), 10);
        }

        // Wrap elements
        if (options.itemsOnSlide) {
          var itemsOnSlide = options.itemsOnSlide;
          var $owlCarouselItems = $owlCarousel.children();
          var slidesCount = Math.ceil($owlCarouselItems.length / itemsOnSlide);

          for (var slideNumber = 0; slideNumber < slidesCount; slideNumber++) {
            $owlCarouselItems.slice(slideNumber * itemsOnSlide, (slideNumber + 1) * itemsOnSlide).wrapAll('<div class="slide"></div>');
          }
        }

        $owlContent.owlCarousel(options);

        return true;
      });
    }
  };
})(jQuery);
