(function ($) {
  Drupal.behaviors.base = {
    attach: function (context, settings) {
      // jQuery UI Tabs
      if ($.fn.tabs) {
        $('.base-tabs:not(.ui-tabs)', context).tabs();
      }
    }
  };

  Drupal.base = {
    /**
     * Check support WebP format.
     */
    checkWebpSupport: function (callback) {
      var supportWebp = $.cookie ? $.cookie('has_webp') : null;

      if ($.cookie && (supportWebp = $.cookie('has_webp')) !== null) {
        callback(supportWebp);
      }
      else {
        var img = new Image();
        img.src = 'data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA';
        img.onload = img.onerror = function () {
          supportWebp = (img.height === 2);

          if ($.cookie) {
            $.cookie('has_webp', Number(supportWebp));
          }

          callback(supportWebp);
        };
      }
    }
  };

  // Check support WebP without wait dom ready
  Drupal.base.checkWebpSupport(function (result) {
    if (!result) {
      $('html').addClass('no-webp');
    }
  });

  if ($.ui && $.ui.dialog && $.ui.dialog.prototype && $.ui.dialog.prototype._focusTabbable) {
    $.ui.dialog.prototype._focusTabbable = $.noop;
  }
})(jQuery);
