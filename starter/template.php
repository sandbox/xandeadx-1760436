<?php

/**
 * Implements hook_views_post_render().
 */
function hook_views_post_render(&$view, &$output, &$cache) {
  if (isset($view->execute_time)) {
    $execute_time = intval($view->execute_time * 100000) / 100;
    $render_time = intval($view->render_time * 100000) / 100;
    $output .= "\n<!-- Query time: $execute_time ms, Render time: $render_time ms -->";
  }
}
