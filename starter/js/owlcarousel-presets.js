Drupal.owlCarouselPresets = {
  slideshow: {
    items: 1,
    loop: true,
    mouseDrag: false,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    nav: false,
    autoplay: true,
    autoplayHoverPause: true,
    autoplayTimeout: 5000
  },
  slider3: {
    items: 3,
    slideBy: 3,
    margin: 30,
    mouseDrag: false,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    nav: true,
    navText: ['←', '→'],
    navSpeed: 150
  }
};
